
import numpy as np
BOARD_COLS = 3
BOARD_ROWS = 3
states_value = {}

def play():
    board = np.zeros((BOARD_ROWS, BOARD_COLS))
    print("Board state before play turn")
    draw_board(board)
    board[0, 0] = 1
    print("Board state after play turn")
    draw_board(board)
    board[0, 1] = -1
    print("Board state after 2nd play turn")
    draw_board(board)

def draw_board(board):
    #global board
    global states_value
    print("Board: ", board)
    boardHash = str(board.reshape(BOARD_COLS * BOARD_ROWS))
    print("Board Hash: ", boardHash)
    print("Positions: ", availablePositions(board))
    print("State-value: ", states_value.get(boardHash))

def availablePositions(board):
    # Append the vacant positions after play turn
    positions = []
    for i in range(BOARD_ROWS):
        for j in range(BOARD_COLS):
            if board[i, j] == 0:
                positions.append((i, j))
    return positions

play()
