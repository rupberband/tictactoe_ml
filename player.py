
import numpy as np

def _init_(self, name, exp_rate=0.3):
    self.name = name
    self.states = [] # record all positions taken
    self.lr = 0.2
    self.exp_rate = exp_rate
    self.decay_gamma = 0.9
    self.states_value = {} # state -> value

def chooseAction(self, positions, current_board, symbol):
    if np.random.uniform(0, 1) <= self.exp_rate:
        # take random action
        idx = np.random.choice(len(positions))
        action = positions[idx]
    else:
        value_max = -999
        for p in positions:
            next_board = current_board.copy()
            next_board[p] = symbol
            next_boardHash = self.getHash(next_board)
            value = 0 if self.states_value.get(next_boardHash) is None else self.states_value.get(next_boardHash)
            # print("Value", value)
            if value >= value_max:
                value_max = value
                action = p
    # print("{} takes action {}".format(self.name, action))
    return action

def feedReward(self, reward):
    for st in reversed(self.states):
        if self.states_value.get(st) is None:
            self.states_value[st] = 0
        self.states_value[st] += self.lr * (self.decay_gamma * reward - self.states_value[st])
        reward = self.states_value[st]
