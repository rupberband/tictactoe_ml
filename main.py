import numpy as np

board = [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
player = 'X'
BOARD_ROWS = 3
BOARD_COLS = 3

def play():
    board = np.zeros((BOARD_ROWS, BOARD_COLS))
    print("Board: ", board)
    boardHash = str(board.reshape(BOARD_COLS * BOARD_ROWS))
    print("Board Hash: ", boardHash)

    board[1, 1] = 1

    positions = []
    for i in range(BOARD_ROWS):
        for j in range(BOARD_COLS):
            if board[i, j] == 0:
                positions.append((i, j))

    print("Positions: ", positions)
    """
    while(True):
        drawBoard()
        askPosition()
        changePlayer()
    """

def drawBoard():
    print("Board")
    print("| " + board[0] + " | " + board[1] + " | " + board[2] + " |")
    print("-------------")
    print("| " + board[3] + " | " + board[4] + " | " + board[5] + " |")
    print("-------------")
    print("| " + board[6] + " | " + board[7] + " | " + board[8] + " |")

def askPosition():
    print("Choose position [1-9]: ")
    position = int(input())
    board[position-1] = player

def changePlayer():
    global player
    if player == 'X':
        player = 'O'
    else:
        player = 'X'


"""

def moveCheck():

def winCheck():

def reset():
"""

play()
